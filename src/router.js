import VueRouter from "vue-router";
import GetWeather from "@/components/GetWeather.vue";
import StorageDemo from "@/components/StorageDemo.vue";
import UserLogin from "@/components/UserLogin.vue";
import ChangeMenuCollapse from "@/components/ChangeMenuCollapse.vue";
import ShowDialog from "@/components/ShowDialog.vue";
import NavOtherMenu from "@/components/NavOtherMenu.vue";
import NavOtherApp from "@/components/NavOtherApp.vue";

const routes = [
    {
        path:"/get-weather",
        name:'GetWeather',
        component:GetWeather
    },
    {
        path:"/user-login",
        name:'UserLogin',
        component:UserLogin
    },
    {
        path:"/storage-demo",
        name:'存储',
        component:StorageDemo
    },
    {
        path:"/change-menu-collapse",
        name:'菜单展开',
        component:ChangeMenuCollapse
    },
    {
        path:"/show-dialog",
        name:'弹窗',
        component:ShowDialog
    },
    {
        path:"/nav-other-menu",
        name:'跳转菜单',
        component:NavOtherMenu
    },
    {
        path:"/nav-other-app",
        name:'跳转应用',
        component:NavOtherApp
    },
    {
        path: "/iframe-demo",
        name: "iframe",
        component: ()=>import("@/components/IframeDemo.vue")
    }
]

const router = new VueRouter({
    mode:"history",
    base: window.__MICRO_APP_BASE_ROUTE__ || process.env.BASE_URL,
    routes
})

export default router
