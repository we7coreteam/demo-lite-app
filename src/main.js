import Vue from 'vue'
import App from './App.vue'
import router from "@/router";
import VueRouter from "vue-router";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(ElementUI)

// 全局监听控制台下发path路径变化以响应菜单路由跳转
// 判断是不是独立系统环境
if (window.__MICRO_APP_ENVIRONMENT__) {
  window.microApp.addDataListener((data) => {
    // 当控制台下发path时进行跳转
    // 当路由为hash模式时，会带上/#/，需要自行处理
    if(data.type=='route' && data.data){
        router.push(data.data);
    }
  })
}

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
