(function(){
    msg();

    window.addEventListener("load",function(){
        setA();
        var observe=new MutationObserver(setA);
        observe.observe(document.body,{childList:true, subtree:true});
    });

    function setA(){
        var a = document.querySelectorAll("a");
        for(var i=0; i<a.length; i++){
            a[i].setAttribute("target","_self");
        }
    }

	function msg(){
		if(!window.parent){return}
		window.w7 = {
			storage_key: '',
            ready(callback){
                w7Set();
                window.parent.postMessage({type:"get_storage_key"},"*");
                window.addEventListener("w7_get_storage_key",function(e){
                    window.w7.storage_key = e.data;
                    callback && callback();
                },{once:true});
            },
		};

        window.addEventListener("message",(e)=>{
            let msg = e.data;
            if(!msg.w7 || !msg.type){return}
            let event = new Event(msg.type);
            event.data = msg.data;
            window.dispatchEvent(event);
        });

        function w7Set(){
            if(!window.w7){window.w7 = {}};
            window.w7.setStorage = ({key, value}) => {
                return localStorage.setItem(((window.w7.storage_key || '') + key), value)
            }
            window.w7.getStorage = (key) => {
                return localStorage.getItem(((window.w7.storage_key || '') + key))
            }
            window.w7.removeStorage = (key) => {
                return localStorage.removeItem(((window.w7.storage_key || '') + key))
            }
            window.w7.clearStorage = ()=>{
                if(window.w7.storage_key) {
                    for(let i in localStorage) {
                        if(i.indexOf(window.w7.storage_key)>-1) {localStorage.removeItem(i)}
                    }
                } else {
                    return localStorage.clear()
                }
            }
            // 支付
            window.w7.pay = (ticket,callback)=>{
                window.parent.postMessage({type:"pay", ticket:ticket}, "*");
                window.addEventListener('w7_pay',callback,{once:true});
            }
			window.w7.payment = (data,callback)=>{
				// origin_appid 支付系统id,  origin_pay_appid APPID,  goods_ids 商品ID数组,  account_id 户名id,  version 版本
				window.parent.postMessage({type:"payment", data: data}, "*");
                window.addEventListener('w7_payment',callback,{once:true});
			}
            // 登录
            window.w7.login = (callback)=>{
                window.parent.postMessage({type:"login"}, "*");
                window.addEventListener('w7_login',callback,{once:true});
            }
            // 获取ticket
            window.w7.jsTicket = (callback)=>{
                window.parent.postMessage({type:"jsTicket"}, "*");
                window.addEventListener('w7_jsTicket',callback,{once:true});
            }
            // 获取实名
            window.w7.realname = (callback)=>{
                window.parent.postMessage({type:"realname"}, "*");
                window.addEventListener('w7_realname',callback,{once:true});
            }
            // 模块信息
            window.w7.getModuleInfo = (callback)=>{
                window.parent.postMessage({type:"getModuleInfo"}, "*");
                window.addEventListener('w7_getModuleInfo',callback,{once:true});
            }
            // 菜单收起展开
            window.w7.changeMenuCollapse = (isOpen)=>{
                window.parent.postMessage({type:"changeMenuCollapse", value:isOpen}, "*");
            }
            // 打开邀请界面
            window.w7.inviteUser = ()=>{
                window.parent.postMessage({type:"inviteUser"}, "*");
            }
			window.w7.loaded = ()=>{
				window.parent.postMessage({loaded:location.href}, '*');
			}
			// 打开其他应用
			window.w7.navigate = (obj)=>{
				// obj: { modulename, type, route, sitekey }
				window.parent.postMessage({type:"appcont", data:obj},'*');
			}
			// （其他应用）返回原本应用
			window.w7.closeCard = (data)=>{
				window.parent.postMessage({type:"returnAppcont", data:data},'*');
			}
        }
	}
})();