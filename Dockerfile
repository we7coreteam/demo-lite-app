FROM node:lts-alpine3.18

ENV WEB_PATH /home
WORKDIR $WEB_PATH
COPY . .

RUN echo "https://mirrors.cloud.tencent.com/alpine/v3.18/main" > /etc/apk/repositories && \
    echo "https://mirrors.cloud.tencent.com/alpine/v3.18/community" >> /etc/apk/repositories

RUN apk add --no-cache nginx
RUN npm install && npm run build
RUN mkdir -p /etc/nginx/http.d/
RUN echo -e " \
server { \
        listen 8085; \n \
        server_name lite-app-sanbox.w7.cc; \n \
        location / { \n \
                proxy_pass http://127.0.0.1:8084; \n \
        } \n \
        location /demo1 { \n \
                proxy_pass http://127.0.0.1:8080; \n \
        } \n \
        access_log off; \n \
}" > /etc/nginx/http.d/default1.conf

RUN echo -e " \
    nginx \n \
    ./server/rangine server:start -f ./server/config.yaml \n \
    " > /home/start.sh

RUN chmod +x ./server/rangine && chmod +x /home/start.sh
CMD ["/home/start.sh"]

EXPOSE 8085